"""
Example of Logistic Regression using MLFlow

Author: William Arias @warias
Version: 1.0.0

"""

import mlflow
import numpy as np
import pandas as pd
import mlflow.sklearn
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score, plot_roc_curve


# Dataset

DATA = "data/creditcard.csv"

# Data processing functions


def train_test_data_split(data, test_size=0.2, random_state=1):

    """
    :param data: CSV - dataset that needs to be split
    :param random_state: int - set this parameter ro repeat experiment
    :param test_size: Float - percentage of data to be used for testing
    :return: training data and test data splits
    """

    training_data, test_data = train_test_split(data, test_size=test_size, random_state=random_state)
    return training_data, test_data


def data_concatenator(dataframe_1, dataframe_2):
    """
        Concatenates and drops the Column containing the label or class to be learnt
        :param dataframe_1: dataframe created by the train_test split function
        :param dataframe_2: int - dataframe created by the train_test split functiont
        :return: training data and test data splits
    """
    df_concatenated = pd.concat((dataframe_1, dataframe_2))
    y_labels = np.array(df_concatenated["Class"])
    df_concatenated = df_concatenated.drop("Class", axis=1)
    return df_concatenated, y_labels


# Data training, evaluation functions

def train(sk_model, x_train, y_train):
    """
    :param sk_model: The model that will be used from sklearn
    :param x_train: data for training
    :param y_train: classes or labels for each data point from training set
    :return:
    """
    sk_model = sk_model.fit(x_train, y_train)
    training_acc = sk_model.score(x_train, y_train)
    mlflow.log_metric("Training accuracy", training_acc)
    print(f"Training accuracy: {training_acc:0.3%}")


def evaluate(sk_model, x_test, y_test):
    """
    :param sk_model: sklearn model that will be tested
    :param x_test: testing data
    :param y_test: testing classes
    :return:
    """
    eval_accuracy = sk_model.score(x_test, y_test)
    predictions = sk_model.predict(x_test)
    auc_score = roc_auc_score(y_test, predictions)
    mlflow.log_metric("evaluation_accuracy", eval_accuracy)
    mlflow.log_metric("auc_score", auc_score)
    print(f"auc Score: {auc_score:0.3%}")
    print(f"eval_accuracy: {eval_accuracy:0.3%}")


""" 
    Data processing pipeline
    
    1. Read and load the dataset in a dataframe
    2. Split the whole dataset in OK/NO-OK transactions
    3. Create training/test/Validation data splits for each category (OK/NO-OK)
    4. Assemble training dataset, test and validation by concatenating the splits created in step 3
    5. Scale and transform the data by removing the mean and scaling to unit variance 
    6. Train, evaluate and log with MlFlow Wrapper

"""

# 1. Read and load the dataset in a dataframe

df = pd.read_csv(DATA).drop("Time", axis=1)

# 2. Split the whole dataset in OK/NO-OK transactions

ok_transaction = df[df.Class == 0].sample(frac=0.5, random_state=1).reset_index(drop=True)
nok_transaction = df[df.Class == 1]

# 3. Create training/test/Validation data splits for each category (OK/NO-OK)

ok_transaction_trn, ok_transaction_tst = train_test_data_split(ok_transaction)
nok_transaction_trn, nok_transaction_tst = train_test_data_split(nok_transaction)

ok_transaction_trn, ok_transaction_validate = train_test_data_split(ok_transaction_trn, test_size=0.25)
nok_transaction_trn, nok_transaction_validate = train_test_data_split(nok_transaction_trn, test_size=0.25)

#  4. Assemble training dataset, test and validation by concatenating the splits created in step 3

x_train, y_train = data_concatenator(ok_transaction_trn, nok_transaction_trn)
x_test, y_test = data_concatenator(ok_transaction_tst, nok_transaction_tst)
x_validate, y_validation = data_concatenator(ok_transaction_validate, nok_transaction_validate)

#  5. Scale and transform the data by removing the mean and scaling to unit variance

scaler = StandardScaler()
scaler.fit(pd.concat((ok_transaction, nok_transaction)).drop("Class", axis=1))
x_train = scaler.transform(x_train)
x_test = scaler.transform(x_test)
x_validate = scaler.transform(x_validate)


#  6. Train, evaluate and log with MlFlow Wrapper

sk_model = LogisticRegression(random_state=None, max_iter=300, solver='newton-cg')
mlflow.set_experiment("log_reg_credit_fraud")
with mlflow.start_run():
    train(sk_model, x_train, y_train)
    evaluate(sk_model, x_test, y_test)
    mlflow.sklearn.log_model(sk_model, "logistic_reg_model")
    print("Model run: ", mlflow.active_run().info.run_uuid)
mlflow.end_run()
